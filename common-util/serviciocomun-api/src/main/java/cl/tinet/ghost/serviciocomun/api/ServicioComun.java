package cl.tinet.ghost.serviciocomun.api;

import aQute.bnd.annotation.ProviderType;

/**
 * Servicio utilitario comun.
 * 
 * @author Francisco Mendoza C.
 */
@ProviderType
public interface ServicioComun {
	
	/**
	 * Modifica el valor del texto enviado.
	 * @param valor Valor del texto a procesar.
	 * @return texto procesado.
	 */
	public String procesaTexto(String valor);
	
	/**
	 * Utilitario para generar proceso.
	 * @param id Identificador.
	 */
	public void procesar(Long id);
}