# Servicio OSGi para usar en un Portlet

## 1. Service API (interfaz)

Crear el proyecto con el comando `blade`

```shell
$> blade create -t api -p cl.tinet.ghost.serviciocomun -c ServicioComun serviciocomun-api
```

Modificar archivo `build.gradle` del proyecto y agregar la dependencia

```gradle
    compileOnly group: "biz.aQute.bnd", name: "biz.aQute.bndlib", version: "3.1.0"
```

Modificar clase `ServicioComun.java` y agregar la anotación que permite definir como proveedor de servicio _osgi_: `@ProviderType`

```java
import aQute.bnd.annotation.ProviderType;

@ProviderType
public interface ServicioComun {
```

Y agregar las firmas de los métodos que proveerá el servicio dentro de la interfaz.

En caso de ser necesario se define una interfaz con constantes para utilizar en los demás proyectos que usen el servicio.

## 2. Service provider (implementación)

Crear el proyecto con el comando `blade`

```shell
blade create -t service -p cl.tinet.ghost.serviciocomun.impl -s cl.tinet.ghost.serviciocomun.api.ServicioComun serviciocomun-impl
```

Modificar archivo `build.gradle` y agregar la referencia al proyecto __serviciocomun-api__.

```gradle
    compileOnly project (':modules:common-util:serviciocomun-api')
```

Agregar implementación a de los métodos que provee el servicio, además métodos para trazar la inicialización de los servicios (`@Activate, @Modified, @Deactivate`).

```java
    private static Log LOG = LogFactoryUtil.getLog(ServiciocomunImpl.class);

    @Activate
    @Modified
    void activar() throws Exception {
        LOG.info("[ServiciocomunImpl] Servicio activado");
    }

    @Deactivate
    void desactivar() throws Exception {
        LOG.info("[ServiciocomunImpl] Servicio desactivado");
    }
```

## 3. Consumer (Portlet)

Crear el proyecto __Portlet__ con el comando `blade`

```shell
blade create -t mvc-portlet -p cl.tinet.ghost.serviciocomun.web serviciocomun-web
```

Modificar archivo `build.gradle` y agregar la referencia al proyecto __serviciocomun-api__.

```gradle
    compileOnly project (':modules:common-util:serviciocomun-api')
```

Agregar referencia en la clase `java` del portlet.

```java
    @Reference
    private ServicioComun servicio;
```

---------------

## Proyecto de ejemplo

### Proyecto: __serviciocomun-api__

* `build.gradle`

```gradle
dependencies {
    compileOnly group: "org.osgi", name: "org.osgi.core", version: "6.0.0"
    compileOnly group: "org.osgi", name: "org.osgi.service.component.annotations", version: "1.3.0"
    compileOnly group: "biz.aQute.bnd", name: "biz.aQute.bndlib", version: "3.1.0"
}
```

* `ServicioComun.java`

```java
package cl.tinet.ghost.serviciocomun.api;

import aQute.bnd.annotation.ProviderType;

/**
 * Servicio utilitario comun.
 *
 * @author Francisco Mendoza C.
 */
@ProviderType
public interface ServicioComun {

    /**
     * Modifica el valor del texto enviado.
     * @param valor Valor del texto a procesar.
     * @return texto procesado.
     */
    public String procesaTexto(String valor);

    /**
     * Utilitario para generar proceso.
     * @param id Identificador.
     */
    public void procesar(Long id);
}
```

* `ConstantesComunes.java`

```java
package cl.tinet.ghost.serviciocomun.api;

public interface ConstantesComunes {

    public static final String CONSTANTE = "valor de la constante";

    public static final String CAMPO = "campo";
}
```

### Proyecto: __serviciocomun-impl__

* `build.gradle`

```gradle
dependencies {
    compileOnly group: "com.liferay.portal", name: "com.liferay.portal.kernel", version: "3.0.0"
    compileOnly group: "org.osgi", name: "org.osgi.service.component.annotations", version: "1.3.0"
    compileOnly project (':modules:common-util:serviciocomun-api')
}
```

* `ServiciocomunImpl.java`

```java
package cl.tinet.ghost.serviciocomun.impl;

import cl.tinet.ghost.serviciocomun.api.ServicioComun;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Modified;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

/**
 * @author Francisco Mendoza C.
 */
@Component(
    immediate = true,
    property = {
        "service.ranking:Integer=100"
    },
    service = ServicioComun.class
)
public class ServiciocomunImpl implements ServicioComun {

    /**
     * Logger de la clase.
     */
    private static Log LOG = LogFactoryUtil.getLog(ServiciocomunImpl.class);

    @Activate
    @Modified
    void activar() throws Exception {
        LOG.info("[ServiciocomunImpl] Servicio activado");
    }

    @Deactivate
    void desactivar() throws Exception {
        LOG.info("[ServiciocomunImpl] Servicio desactivado");
    }

    @Override
    public String procesaTexto(String valor) {
        LOG.info("[ServiciocomunImpl] Parametro recibido: " + valor);
        return valor.toUpperCase();
    }

    @Override
    public void procesar(Long id) {
        LOG.info("[ServiciocomunImpl] Identificador recibido: " + id);
        LOG.info("[ServiciocomunImpl] Hacer algo con el id");        
    }
}
```

### Proyecto: __serviciocomun-web__

* `build.gradle``

```gradle
dependencies {
    compileOnly group: "com.liferay.portal", name: "com.liferay.portal.kernel", version: "3.0.0"
    compileOnly group: "com.liferay.portal", name: "com.liferay.util.taglib", version: "3.1.2"
    compileOnly group: "javax.portlet", name: "portlet-api", version: "3.0.0"
    compileOnly group: "javax.servlet", name: "javax.servlet-api", version: "3.0.1"
    compileOnly group: "jstl", name: "jstl", version: "1.2"
    compileOnly group: "org.osgi", name: "org.osgi.service.component.annotations", version: "1.3.0"
    compileOnly project (':modules:common-util:serviciocomun-api')
}
```

* `ServiciocomunWebPortlet.java`

```java
package cl.tinet.ghost.serviciocomun.web.portlet;

import cl.tinet.ghost.serviciocomun.api.ConstantesComunes;
import cl.tinet.ghost.serviciocomun.api.ServicioComun;
import cl.tinet.ghost.serviciocomun.web.constants.ServiciocomunWebPortletKeys;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.util.ParamUtil;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.Portlet;
import javax.portlet.ProcessAction;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

/**
 * Portlet de prueba para invocar el servicio OSGi.
 * 
 * @author Francisco Mendoza C.
 */
@Component(
    immediate = true,
    property = {
        "com.liferay.portlet.display-category=category.sample",
        "com.liferay.portlet.instanceable=true",
        "javax.portlet.init-param.template-path=/",
        "javax.portlet.init-param.view-template=/view.jsp",
        "javax.portlet.name=" + ServiciocomunWebPortletKeys.ServiciocomunWeb,
        "javax.portlet.resource-bundle=content.Language",
        "javax.portlet.security-role-ref=power-user,user"
    },
    service = Portlet.class
)
public class ServiciocomunWebPortlet extends MVCPortlet {

    /**
     * Logger de la clase.
     */
    private static final Log LOGGER = LogFactoryUtil.getLog(ServiciocomunWebPortlet.class);

    /**
     * Inyeccion del servicio OSGi.
     */
    @Reference
    private ServicioComun servicio;

    /**
     * 
     * @param request
     * @param response
     */
    @ProcessAction(name = "texto")
    public void accionTexto(ActionRequest request, ActionResponse response) {
        LOGGER.info("accion texto");
        String texto = ParamUtil.getString(request, "texto");

        if (servicio != null) {
            LOGGER.info(servicio.procesaTexto(texto));
        } else {
            LOGGER.info("no hay servicio osgi");
        }

        LOGGER.info(ConstantesComunes.CONSTANTE);
    }

    /**
     *
     * @param request
     * @param response
     */
    @ProcessAction(name = "procesa")
    public void accionProcesa(ActionRequest request, ActionResponse response) {
        LOGGER.info("accion procesa");
        String texto = ParamUtil.getString(request, "texto");

        // Invocar servicio.
        int largo = texto.length();
        servicio.procesar(Long.valueOf(largo));

        LOGGER.info(ConstantesComunes.CAMPO);
    }
}
```

* `view.jsp`

```jsp
<%@ include file="/init.jsp" %>

<portlet:actionURL name="texto" var="textoUrl"/>
<portlet:actionURL name="procesa" var="procesaUrl"/>

<aui:form action="${textoUrl}">
    <aui:input name="texto" type="text" label="Texto a enviar" />
    <aui:button name="textoButton" type="button" value="Enviar texto" />
    <aui:button name="procesaButton" type="button" value="Procesar texto" />
</aui:form>

<aui:script use="aui-base,aui-request">

var miformulario = AUI().one('#<portlet:namespace />fm');

miformulario.one('#<portlet:namespace />textoButton').on('click', function(event) {
    console.log("enviar texto mayusculas...");
    AUI().io.request('${textoUrl}', {
        method: 'POST',
        form: {id: '<portlet:namespace />fm'},
        on: {
            failure: function() { console.log("Ocurrio un error..."); },
            success: function(event, id, obj) { console.log("Formulario enviado!."); }
        }
    });
});

miformulario.one('#<portlet:namespace />procesaButton').on('click', function(event) {
    console.log("enviar texto minusculas...");
    AUI().io.request('${procesaUrl}', {
        method: 'POST',
        form: {id: '<portlet:namespace />fm'},
        on: {
            failure: function() { console.log("Ocurrio un error..."); },
            success: function(event, id, obj) { console.log("Formulario enviado!."); }
        }
    });
});

</aui:script>
```

## Referencias

[Documentación utilizada](https://portal.liferay.dev/docs/7-0/tutorials/-/knowledge_base/t/osgi-and-modularity)
