<%@ include file="/init.jsp" %>

<portlet:actionURL name="texto" var="textoUrl"/>
<portlet:actionURL name="procesa" var="procesaUrl"/>

<aui:form action="${textoUrl}">
    <aui:input name="texto" type="text" label="Texto a enviar" />
    <aui:button name="textoButton" type="button" value="Enviar texto" />
	<aui:button name="procesaButton" type="button" value="Procesar texto" />
</aui:form>

<aui:script use="aui-base,aui-request">

var miformulario = AUI().one('#<portlet:namespace />fm');

miformulario.one('#<portlet:namespace />textoButton').on('click', function(event) {
	console.log("enviar texto mayusculas...");
	AUI().io.request('${textoUrl}', {
		method: 'POST',
		form: {id: '<portlet:namespace />fm'},
		on: {
			failure: function() { console.log("Ocurrio un error..."); },
			success: function(event, id, obj) { console.log("Formulario enviado!."); }
		}
	});
});

miformulario.one('#<portlet:namespace />procesaButton').on('click', function(event) {
	console.log("enviar texto minusculas...");
	AUI().io.request('${procesaUrl}', {
		method: 'POST',
		form: {id: '<portlet:namespace />fm'},
		on: {
			failure: function() { console.log("Ocurrio un error..."); },
			success: function(event, id, obj) { console.log("Formulario enviado!."); }
		}
	});
});

</aui:script>