package cl.tinet.ghost.serviciocomun.web.portlet;

import cl.tinet.ghost.serviciocomun.api.ConstantesComunes;
import cl.tinet.ghost.serviciocomun.api.ServicioComun;
import cl.tinet.ghost.serviciocomun.web.constants.ServiciocomunWebPortletKeys;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.util.ParamUtil;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.Portlet;
import javax.portlet.ProcessAction;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

/**
 * Portlet de prueba para invocar el servicio OSGi.
 * 
 * @author Francisco Mendoza C.
 */
@Component(
	immediate = true,
	property = {
		"com.liferay.portlet.display-category=category.sample",
		"com.liferay.portlet.instanceable=true",
		"javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.name=" + ServiciocomunWebPortletKeys.ServiciocomunWeb,
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user"
	},
	service = Portlet.class
)
public class ServiciocomunWebPortlet extends MVCPortlet {
	
	/**
	 * Logger de la clase.
	 */
	private static final Log LOGGER = LogFactoryUtil.getLog(ServiciocomunWebPortlet.class);

	/**
	 * Inyeccion del servicio OSGi.
	 */
	@Reference
	private ServicioComun servicio;

	/**
	 * 
	 * @param request
	 * @param response
	 */
	@ProcessAction(name = "texto")
	public void accionTexto(ActionRequest request, ActionResponse response) {
		LOGGER.info("accion texto");
		String texto = ParamUtil.getString(request, "texto");

		if (servicio != null) {
			LOGGER.info(servicio.procesaTexto(texto));
		} else {
			LOGGER.info("no hay servicio osgi");
		}

		LOGGER.info(ConstantesComunes.CONSTANTE);
	}
	
	/**
	 * 
	 * @param request
	 * @param response
	 */
	@ProcessAction(name = "procesa")
	public void accionProcesa(ActionRequest request, ActionResponse response) {
		LOGGER.info("accion procesa");
		String texto = ParamUtil.getString(request, "texto");
		
		// Invocar servicio.
		int largo = texto.length();
		servicio.procesar(Long.valueOf(largo));
		
		LOGGER.info(ConstantesComunes.CAMPO);
	}
}
