package cl.tinet.ghost.serviciocomun.impl;

import cl.tinet.ghost.serviciocomun.api.ServicioComun;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Modified;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

/**
 * @author Francisco Mendoza C.
 */
@Component(
	immediate = true,
	property = {
		"service.ranking:Integer=100"
	},
	service = ServicioComun.class
)
public class ServiciocomunImpl implements ServicioComun {

	/**
	 * Logger de la clase.
	 */
	private static Log LOG = LogFactoryUtil.getLog(ServiciocomunImpl.class);

	@Activate
	@Modified
	void activar() throws Exception {
		LOG.info("[ServiciocomunImpl] Servicio activado");
	}

	@Deactivate
	void desactivar() throws Exception {
		LOG.info("[ServiciocomunImpl] Servicio desactivado");
	}
	
	@Override
	public String procesaTexto(String valor) {
		LOG.info("[ServiciocomunImpl] Parametro recibido: " + valor);
		return valor.toUpperCase();
	}

	@Override
	public void procesar(Long id) {
		LOG.info("[ServiciocomunImpl] Identificador recibido: " + id);
		LOG.info("[ServiciocomunImpl] Hacer algo con el id");		
	}
}
